##TODO


Required tools for running this demo:


- terraform configured for GCP.
- ansbible
- docker
- jenkins instance
- kubectl client


What this tool does?
It creates a K8S cluster on GCP as a remote development environment. 
Builds sample application with docker , deploys with jenkins - provides multibranch approach.
Shows demo of simple CI/CD with Jenkins when there is a new commit on repository.


In order to run this demo you require few things.


1. change var.tf terraform variable file for your environment
you need a gcp.key file which you can obtain from GCP Console

"From the service account key page in the Cloud Console choose an existing account, or create a new one. Next, download the JSON key file. Name it something you can remember, and store it somewhere secure on your machine."

2. set a "ssh_user" valid for your gcp project.

3. create a ssh-key to access your machines - assing file path to variable -> ssh_pub_key_file

4. create a jenkins instance in your localmachine on port 8080. make sure "Docker Pipeline" and "Docker plugin" are installed
  provide credentials for dockerhub.

5. ansbile playbook copies kubeconfing file to jenkins default home directory - no need to configure kubernetes credentials.

6. change ansible_ssh_private_key_file variable on ansible playbooks for your environment to access machines.

7. set up Jenkinsfile-mbranch according to your environment , change dockerreg and image parameters

8. create a multibranch jenkins pipeline -use  file called jenkinsjob.xml to restore job.

9. run createcluster.sh script to create cluster and run application on it . kubeproxy will be provided and application will be accessible on local port 2020 
