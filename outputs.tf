### The Ansible inventory file
resource "local_file" "AnsibleInventory" {
 content = templatefile("inventory.tmpl",
 {
  master-ip = google_compute_instance.master.network_interface.0.network_ip,
  worker-ip = google_compute_instance.workers.*.network_interface.0.network_ip,
  ssh_user = var.ssh_user,
  master-pub-ip = google_compute_instance.master.network_interface.0.access_config.0.nat_ip,
  worker-pub-ip = google_compute_instance.workers.*.network_interface.0.access_config.0.nat_ip
 }
 )
 filename = "inventory"
}

