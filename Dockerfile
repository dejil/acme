FROM  alpine:3.13.0


WORKDIR /app

EXPOSE 3000


# Install python/pip
ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3 python3-dev mariadb-dev gcc musl-dev && ln -sf python3 /usr/bin/python

RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools

COPY requirements.txt /app
RUN pip install -r requirements.txt

COPY  myapp.py /app

RUN chgrp -R 0 /app && \
    chmod -R g=u /app

RUN adduser -H -D  -u 2000 -G root -s /bin/sh --skel /dev/null myuser

USER myuser
CMD python myapp.py
