terraform apply -auto-approve
ansible-playbook -i inventory install-docker.yaml
ansible-playbook -i inventory install-kubernetes-prereq.yaml
ansible-playbook -i inventory install-k8s-master.yaml
ansible-playbook -i inventory install-k8s-workers.yaml
kubectl create -f k8s-manifests -n acme --kubeconfig=/var/lib/jenkins/.kube/config
until [ $(kubectl get pods -n acme --kubeconfig=/var/lib/jenkins/.kube/config | grep "1/1" | wc -l) -eq 2 ]; do  echo "waiting pods up and ready" && sleep 5; done
kubectl port-forward service/myapp -n acme --kubeconfig=/var/lib/jenkins/.kube/config 2020:3000 &
sleep 5 #wait for bg to complate
curl localhost:2020
