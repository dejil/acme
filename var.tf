variable "project" {
  default = "acme-318315"
}

variable "credentials_file" {
  default = "gcp.key" 
}

variable "region" {
  default = "us-central1"
}

variable "zone" {
  default = "us-central1-a"
}
variable "osimage" {
  default = "ubuntu-os-cloud/ubuntu-1804-lts"
}
variable "boot_disk_size" {
  default = "30"
}
variable "ssh_user" {
  default = "fdpicka"
}
variable "ssh_pub_key_file"{
  default = "/root/.ssh/id_rsa.pub"
}
