provider "google" {
  credentials = file(var.credentials_file)
  project     = var.project
  region      = var.region
  zone        = var.zone
}


resource "google_compute_instance" "master" {
  name         = "master"
  machine_type = "e2-highmem-2"
  tags = ["master"]

  boot_disk {
    initialize_params {
      size = var.boot_disk_size
      image = var.osimage
    }
  }

  network_interface {
    # A default network is created for all GCP projects
    network = "default"
    access_config {
    }
  }
  metadata = {
    ssh-keys = "${var.ssh_user}:${file(var.ssh_pub_key_file)}"
 }
}



resource "google_compute_instance" "workers" {
  count = "2"
  name         = "worker-${count.index}"
  machine_type = "e2-highmem-2"

  boot_disk {
    initialize_params {
      size = var.boot_disk_size
      image = var.osimage
    }
  }

  network_interface {
    # A default network is created for all GCP projects
    network = "default"
    access_config {
    }
  }
  metadata = {
    ssh-keys = "${var.ssh_user}:${file(var.ssh_pub_key_file)}"
 }
}

resource "google_compute_firewall" "default" {
  name    = "calico-fw-rule"
  network = "default"

  allow {
    protocol = "4"
  }
  source_ranges = ["10.128.0.0/9"]
}

resource "google_compute_firewall" "allow-k8s-api" {
  name    = "allow-k8s-api"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["6443"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["master"]
}
